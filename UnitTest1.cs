using NUnit.Framework;
using HomeworkMedvedev;

namespace TestProject1
{
    public class Tests
    {
        [TestCase(70, 10, true)]
        public void AddTest(int value, int capacity, bool expectedreslt)
        {
            AList1 aList1 = new AList1(capacity);
            bool actresult = aList1.Add(value, capacity);
            Assert.True(expectedreslt);
        }


        [TestCase(5, 10, 0)]
        public void GetTest(int index, int capacity, int expectedreslt)
        {
            AList1 aList1 = new AList1(capacity);
            int actresult = aList1.Get(index);
            Assert.AreEqual(expectedreslt, actresult);
        }

        [TestCase(5, 0)]
        public void SizeTest(int capacity, int expectedreslt)
        {
            AList1 aList1 = new AList1(capacity);
            var actresult = aList1.Size();
            Assert.AreEqual(expectedreslt, actresult);
        }


        [TestCase(1, 5, 10, true)]
        public void Add2Test(int index, int value, int capacity, bool expectedreslt)
        {
            AList1 aList1 = new AList1(capacity);
            bool actresult = aList1.Add2(index, value);
            Assert.True(expectedreslt);
        }

        [TestCase(5, 10, 5)]
        public void RemoveTest(int value, int capacity, int expectedreslt)
        {
            AList1 aList1 = new AList1(capacity);
            var actresult = aList1.Remove(value);
            Assert.AreEqual(expectedreslt, actresult);
        }

        [TestCase(5, 10, 5)]
        public void RemovebyIndexTest(int index, int capacity, int expectedreslt)
        {
            AList1 aList1 = new AList1(capacity);
            var actresult = aList1.Remove(index);
            Assert.AreEqual(expectedreslt, actresult);
        }

        [TestCase(70, 10, true)]
        public void ContainsTest(int value, int capacity, bool expectedresult)
        {
            AList1 aList1 = new AList1(capacity);
            aList1.Contains(value);
            Assert.True(expectedresult);
        }

        [TestCase(10, 3, 40, true)]
        public void SetTest(int capacity, int index, int value, bool expectedresult)
        {
            AList1 aList1 = new AList1(capacity);
            aList1.Set(index, value);
            Assert.True(expectedresult);
        }

        [Test]
        public void RemoveAllTest()
        {
            int capacity = 10;
            AList1 aList1 = new AList1(capacity);
            int[] array = { 10, 20, 47 };
            aList1.RemoveAll(array);
            bool expectedresult = true;
            Assert.True(expectedresult);
        }


        //[Test]
        //public void ToArrayTest()
        //{
        //    int capacity = 10;
        //    int size = 3;
        //    AList1 aList1 = new AList1(capacity);
        //    int[] actual = { 10, 20, 47 };
        //    var expected = aList1.ToArray(size);
        //    Assert.Equals(actual, expected);
        //}


        //[Test]
        //public void Clear()
        //{
        //    int capacity = 10;
        //    AList1 aList1 = new AList1(capacity);
        //    aList1.Clear();
        //    Assert.IsEmpty(aList1);
        //}
    }
}